### Sanity Check
Before you proceed, ensure you have a proper AWS Vault & AWS Config. See `aws-cli.md`

In your terminal, verify these commands succeed when starting from the root of the project:

```
cd cloudformation;
source ./.source_me
cf-deploy roles
```

If you should get the following error:

```
An error occurred (InsufficientCapabilitiesException) when calling the CreateChangeSet operation: Requires capabilities : [CAPABILITY_NAMED_IAM]
```

...this means that the stack is not already present.. and it should be. You can either contact us to reset it, or redeploy yourself with the missing capability directive

#### What are these commands?

Bash functions that simplify aws-vault & terraform operations. They get added when you issue the following in your terminal:
`cd cloudformation && source ./.source_me`

```
# Enables "aws cloudformation deploy"

function cf-deploy {
  profile=$1; shift
  template=$1; shift
  aws-vault exec $profile -- aws cloudformation deploy --stack-name $template --template $template.yml "$@";
}
```
