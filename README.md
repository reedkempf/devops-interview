# DevOps Technical Interview

## Overview
The following exercises are meant to test the ability of Payouts Network's engineering candidates
who are applying for roles that require expertise in modern cloud management workflows.

The exercises below will require creating new resources, managing existing ones, and 
migrating resources from one tool to another. The AWS environments we are providing are pre-provisioned.
If you need to have your environment reset for whatever reason, please reach out to us.
If you fix it yourself, _exercise the honor system and mention it during the review process_.

It's not a problem at all to fix it yourself; those are the types of people we want to work with!

The assignment is designed to take approximately 6-12 hours of dedicated attention.  Given the 
complexity of the assignment, we encourage and appreciate clarifying questions, so please do not 
hesitate to reach out to us.  Ultimately, the accuracy and precision of the work is most important. 

## Tools Installation

We assume you will be using either a MacOS/Linux machine (we recommend MacOS with Homebrew). If you plan to complete the assignment
with a Windows machine, please let us know immediately so that we can adjust our timing expectations
as we know that it will complicate setup.

1. [AWS CLI](https://github.com/aws/aws-cli)
2. [AWS Vault](https://github.com/99designs/aws-vault)
3. [Terraform](https://learn.hashicorp.com/terraform/getting-started/install.html)

## Before You Begin
The assignment requires that you actually deploy infrastructure and commit code to a remote Git repository.

We assume that both of these tasks are trivial.

1. Please signup for a Gitlab.com account and fork this project. Push your work here and ensure your repository remains public.
2. In a separate email you will receive and AWS Account Id, AWS Credentials, and a "AWS account alias"
3. Find and replace the values for `AWS_ACCOUNT_ID` and `AWS_ACCOUNT_ALIAS`
4. The AWS Credentials will involve a setup of `aws-vault`. See `aws-cli.md` for instructions.
5. Once `aws-vault` is working for you, checkout the README files in `cloudformation` and `terraform` and perform the sanity checks

---

## Exercise 1

*Problem*

The team needs a pre-prod environment. They are using a framework to deploy most of application's infrastructure, but they need
some of it pre-provisioned for them. The application is simple: it manages objects in a private S3 bucket.

The CTO wants to closely guard the operations on this bucket and requires that access is logged to a separate bucket as well. Objects
must be encrypted at rest.

*Completion Guidelines*

1. Create a VPC in the `us-east-1` region. The VPC must have the following CIDR: 10.0.0.0/16
2. Create 3 subnets: one public, two private. Use the following CIDRs: 10.0.1.0/24 for the public. 10.0.2.0/24 for the private #1, 10.0.3.0/24 for the private #2
3. Create a NAT gateway and attach 1 private subnet to it, do not attach a NAT to the other private subnet
4. Create a IAM service role that can be assumed by the AWS Lambda Service (it cannot be the service role from `Exercise #2`)
5. Create a private S3 Bucket for the team's application that allows only the lambda service role
6. Create a bucket that will receive access logs from the bucket created in step #5

## Exercise 2
This portion of the assignment will test your ability to manage/migrate to different cloud management tools

*Problem*

The team wants to switch from Cloudformation to Terraform. This must be done in a non-destructive way. For whatever reason,
the team also wishes to attach full Read-Only permissions to `SimpleLambdaServiceRole`

*Completion Guidelines*

1. Locate the `cloudformation` folder
2. Locate the `terraform` folder
2. Run through sanity checks successfully in both `terraform` & `cloudformation` folders
3. Update the `roles.yml` stack so that it can be imported to terraform. Ensure that _if_ the stack was deleted, the underlying resource would not be
4. With the bash shortcuts in `terraform`, import the `SimpleLambdaServiceRole`
5. With the resource imported, attach a policy to the role that grants it Read-Only access on the entire account.

## Exercise 3
- This portion of the assignment will test your ability and knowledge of version control and CI/CD tooling.
- The file you need to modify to complete the assignment: `.gitlab-ci.yml`
- Because we use Gitlab as our code repository, this requires that you create a Gitlab account and that you fork this project.
- [Gitlab CI/CD Reference](https://docs.gitlab.com/ee/ci/yaml/)

*Problem*

We have two stages in our current pipeline which is designed to test for vulnerabilities. The two stages are: `install` and `test`.

- `install` simply installs the NodeJs packages
- `test` will scan for code vulnerabilities and run unit tests
- 'deploy' will deploy the application

The team has decided that the code is becoming hard to read, and now want to enforce code linting before deployments
are initiated.

The team has asked you to enforce this, _regardless if the changes you make result in a broken pipeline_.

*Completion Guidelines*
1. Add a step to the `test` pipeline that will run the command `npm run lint` in the `application` folder
2. Observe the changes and results of the pipeline once committed to the remote repository

## Submission
As you complete the first two exercises, ensure that you leave the infrastructure as you left it when you completed.

For terraform, we are leveraging remote state management so there is no need for you to retain it locally.

In order to fully complete the assignment, create one or several commits to this repository with terse commit messages.

For the last part of the assignment, we expect there may be multiple commits in order to test your configurations. Do not
worry how many commits you have to make, again, correctness is better than elegance.

Email the link to your forked project to the interviewers

**Important!**
Do not commit any files which reveal provided account credentials or other pieces of information sent by us.
