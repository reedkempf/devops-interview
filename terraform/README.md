### Sanity Check
1. Before you proceed, ensure you have a proper AWS Vault & AWS Config. See `aws-cli.md`
2. Make sure you find and replace "AWS_ACCOUNT_ALIAS" with the one that was emailed to you

In your terminal, verify these commands succeed when starting from the root of the project:
```
cd terraform;
source ./.source_me
avt interview init
avt interview apply
```

*Important* If you see an error like this during the sanity check:

```
Error: Error loading state:
    AccessDenied: Access Denied
        status code: 403, request id: A50E79BF1F4C1BFB, host id: sce5gvJoCqTT1r/s/zGG2ERknucanDGK7kAh2bqec8Vf0McNmZdfgcDb+v7W6L5+Pnrkg9MLeM8=
```

This means that you likely did not replace AWS_ACCOUNT_ALIAS in the `main.tf` file. If you cannot escape this error, then you need to "trick" terraform
into reconfiguring the s3 backend. So, to reset, perform the following actions:

1. Locate and _delete_ lines #1-9 in `terraform/main.tf`
2. Run `avt interview init` in the `terraform` folder
3. Restore the lines with `AWS_ACCOUNT_ALIAS` replaced from `terraform/main.tf` file
4. Re-run the sanity check

#### What are these commands?

Bash functions that simplify aws-vault & terraform operations. They get added when you issue the following in your terminal:
`cd terraform && source ./.source_me`

```
# Enables "init", "plan", "apply"

function avt {
  profile=$1; shift
  aws-vault exec $profile -- /usr/local/bin/terraform "$@" -var="profile=$profile" -var-file="./terraform.tfvars";
}

# Lists the terraform state

function avt-list {
  profile=$1; shift
  aws-vault exec $profile -- /usr/local/bin/terraform state list
}

# Removes a resource from the terraform state

function avt-rm {
  profile=$1; shift
  aws-vault exec $profile -- /usr/local/bin/terraform state rm "$@"
}

# Imports a resource into terraform

function avti {
  profile=$1; shift
  aws-vault exec $profile -- /usr/local/bin/terraform import -var="profile=$profile" -var-file="./terraform.tfvars" "$@";
}
```
