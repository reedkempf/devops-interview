terraform {
  backend "s3" {
    bucket          = "payoutsnetwork.terraform"
    region          = "us-east-1"
    # Replace AWS_ACCOUNT_ALIAS with the one emailed to you!
    key             = "interviews/AWS_ACCOUNT_ALIAS/terraform.tfstate"
    encrypt         = true
  }
}

provider "aws" {
  region                  = var.region
  version                 = "~> 2.40"
  profile                 = "payoutsnetwork"
}
